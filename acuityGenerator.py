import os
import numpy as np
import pandas as pd
from skimage.transform import resize
from scipy.interpolate import RegularGridInterpolator
from sklearn.utils.extmath import cartesian
from keras.preprocessing.image import ImageDataGenerator

def resample(input_data, output_shape, slice_dist, total_slice_dist=0.05):
    '''
    slice_dist:
        The distance between slices in the input scan, in meters
    total_slice_dist:
        The estimated world space distance of the region of interest
        in the slice dimension, in meters.
    '''

    # For projects that have less than the desired slice distance, crop the total slice distance to what is possible
    total_raw_distance = input_data.shape[0]*abs(slice_dist)
    if total_raw_distance < total_slice_dist:
        total_slice_dist = total_raw_distance-0.005

    input_shape = input_data.shape
    resize_shape = (input_data.shape[0], *output_shape[1:3])
    resize_img_shape = tuple(output_shape[1:3])
    resized_input = np.zeros(resize_shape)
    for idx, img in enumerate(input_data):
        resized_input[idx] = resize(img, output_shape=resize_img_shape)

    max_slice_count = total_slice_dist/abs(slice_dist)
    pixels_between_slices = (output_shape[0] // max_slice_count)
    pixels_between_slices = max(1, pixels_between_slices)

    input_slice_coords = np.arange(input_shape[0])*pixels_between_slices*2
    output_slice_coords = np.arange(output_shape[0])+int(np.mean(input_slice_coords)*0.5)
    height_coords = np.arange(output_shape[1])
    width_coords = np.arange(output_shape[2])

    args = input_slice_coords, height_coords, width_coords
    interp_points = np.array(args)

    args = output_slice_coords, height_coords, width_coords
    output_points = np.array(args)

    interp = RegularGridInterpolator(
            interp_points,
            resized_input,
            method='linear',
            bounds_error=False,
            fill_value=0)

    point_tuples = np.flip(cartesian(np.flip(output_points)), axis=1)
    interp_values = interp(point_tuples)
    output = np.reshape(interp_values, output_shape, order='F')
    return output

def input(path):
    """
    Load binary .npy file
    """
    return np.load(path, allow_pickle=True)

def output(path, injury_data):
    """
    Generate label for classification
    """
    project = path.split('_')[-2]
    project = project.split('/')[-1]
    disc = path.split('_')[-1][0:-4]
    
    # Filer csv for project id, injury, and disc
    output = injury_data[(injury_data._id==project)&(injury_data.location==disc)]['acuity']
     
    return output.iloc[0]

def preprocess(array, sag_input_shape, ax_input_shape, augment=False):
    """
    All image preprocessing will go here 
    - Extract appropriate slices
    - Resize
    - Impute
    - Potential augmentation
    """   

    """
    Initialize keras generator for the augmentation methods
    Get random transform that will be applied to all images in the series
    """
    if augment == True:
        idg = ImageDataGenerator(
            fill_mode = 'nearest',
            rotation_range = 2,
            width_shift_range = 0.02,
            height_shift_range = 0.02 
        )
        transform = idg.get_random_transform((100,150,1))
        X1 = resample(array[0], (15,100,150,1), array[2])

        for i, img in enumerate(X1):
            X1[i] = idg.apply_transform(img, transform)

    else:
        X1 = resample(array[0], (15,100,150,1), array[2])

    ax_center = int(array[1].shape[0]/2)
    ax_slices = ax_input_shape[-1]

    X2 = []
    for img in array[1]:
        img = resize(img, (200,200))
        X2.append(img)
        
    if len(X2) > 2:
        X2 = X2[:2]

    X2 = np.array(X2)
    X2 = np.rollaxis(X2,0,3)

    return X1, X2

def acuityGenerator(files, injury_data, sag_input_shape, ax_input_shape, batch_size=16, augment=False, resample=False):

    while True:

        # Select files (paths/indices) for the batch
        batch_paths  = np.random.choice(a = files, 
                                        size = batch_size)
        X_sag = np.zeros(sag_input_shape)
        X_ax = np.zeros(ax_input_shape)
        batch_output = [] 
        
        # Read in each input, perform preprocessing and get labels
        for i,input_path in enumerate(batch_paths):
            ipt = input(input_path)
            opt = output(path=input_path, injury_data=injury_data)

            sag, ax = preprocess(ipt, sag_input_shape, ax_input_shape, augment=augment)
    
            X_sag[i] = sag
            X_ax[i] = ax 
            batch_output += [opt]

        # Return a tuple of (input, output) to feed the network
        batch_x = [np.array(X_sag), np.array(X_ax)]
        batch_y = np.array(batch_output)

        yield [batch_x, batch_y]

