import numpy as np
import pandas as pd

LUMBAR_DISCS = ['Disc.L5-S1','Disc.L4-L5','Disc.L3-L4','Disc.L2-L3',
                'Disc.L1-L2','Disc.T12-L1']

df = pd.read_csv('data/injuries.csv')
df = df.drop(labels=[' Severity', 'Unnamed: 6'], axis=1)
df.columns = ['_id', 'location', 'imp', 'acuity', 'hern']

print(df.shape)

# Convert to binary values and drop non-impingement labels
# df = df.dropna(axis=0)

df = df[df.imp == ' Impingement']

df['imp'] = np.where(df.imp == ' Impingement', 1, 0)
df['acuity'] = np.where(df.acuity == ' True', 1, 0)
df['hern'] = np.where(df.hern == ' True', 1, 0)

# Drop to just lumbar cases and strip whitespace
df = df[df.location.map(lambda x: 'L' in x)]
df.location = df.location.map(lambda x: x.strip())
df._id = df._id.map(lambda x: x.strip())

# Add negative cases 
for project in df._id.unique():
    discs = list(df.loc[df._id == project, 'location'])

    null_discs = np.setdiff1d(LUMBAR_DISCS, discs).tolist()

    for disc in null_discs:
        df = df.append({
            '_id': project,
            'location': disc,
            'imp': 0,
            'acuity': 0,
            'hern': 0
        }, ignore_index=True)
    
df = df.drop_duplicates(subset=['_id','location'])

df.to_csv('old_labels.csv')