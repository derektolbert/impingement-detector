import os
import numpy as np
import pandas as pd
import tensorflow as tf
import keras
from keras.models import load_model
from keras.optimizers import Adam
from sklearn.metrics import f1_score, confusion_matrix, accuracy_score
from sklearn.model_selection import train_test_split

# CONSTANTS
batch_size=8
s_shape = (batch_size, 15,100,150,1)
ax_shape = (batch_size, 200,200,2) # Herniation/Acuity default to 200,200,3

"""
Get Model
---------
model_type = 'impingement', 'herniation', or 'acuity'
"""
injury_data = pd.read_csv('old_labels.csv')
model_type = 'impingement'
model_file = 'id_models/L1L2-impingement-0.94%-0416.hdf5'
model = load_model(model_file, compile=False)
model.summary()
model.compile(loss='binary_crossentropy', optimizer=Adam(lr=0.001), metrics=['accuracy'])

if model_type == 'impingement':
    from idGenerator import idGenerator, output, input, preprocess
    """
    Impingement Data
    """   

    labeled_projs = list(injury_data._id)

    # Get list of all positive cases and split 70%,15%,15% for train, validation, test   
    cases = []
    for f in os.listdir('data/staging/'):
        proj = f.split('_')[0]
        disc = f.split('/')[-1].split('_')[1][:-4]
        
        # Only train on labeled projects with labeled discs
        if proj in labeled_projs:
            labeled_discs = list(injury_data.loc[injury_data._id == proj, 'location'])

            if disc in labeled_discs:
                if disc == 'Disc.L1-L2':
                    cases.append('data/staging/' + f)

    train_cases, validation_cases = train_test_split(cases, train_size=0.85, 
                                        random_state=1337) 


    # cases = []
    # for f in os.listdir('data/test/files/'):
    #     proj = f.split('_')[0]
    #     disc = f.split('/')[-1].split('_')[1][:-4]
        
    #     if proj in labeled_projs:
    #         cases.append('data/test/files/' + f)

    # validation_cases = cases


if model_type == 'herniation':
    from hernGenerator import output, input, preprocess
    """
    Herniation data
    """

    labeled_projs = list(injury_data._id)

    # Get list of all positive cases and split 70%,15%,15% for train, validation, test   
    cases = []
    for f in os.listdir('/home/aptus/Clients/Multus/ID_detector/data/staging/'):
        proj = f.split('_')[0]
        disc = f.split('/')[-1].split('_')[1][:-4]
        
        # Only train on labeled projects with labeled discs
        if proj in labeled_projs:
            labeled_discs = list(injury_data.loc[injury_data._id == proj, 'location'])

            if disc in labeled_discs:
                imp = injury_data.loc[(injury_data._id == proj) & (injury_data.location == disc) ,'imp']
                
                if imp.iloc[0] == 1: 
                    cases.append('/home/aptus/Clients/Multus/ID_detector/data/staging/' + f)

    train_cases, validation_cases = train_test_split(cases, train_size=0.85, 
                                        random_state=1337) 

if model_type == 'acuity':
    from acuityGenerator import output, input, preprocess
    """
    Acuity data
    """

    labeled_projs = list(injury_data._id)

    # Get list of all positive cases and split 70%,15%,15% for train, validation, test   
    cases = []
    for f in os.listdir('/home/aptus/Clients/Multus/ID_detector/data/staging/'):
        proj = f.split('_')[0]
        disc = f.split('/')[-1].split('_')[1][:-4]
        
        # Only train on labeled projects with labeled discs
        if proj in labeled_projs:
            labeled_discs = list(injury_data.loc[injury_data._id == proj, 'location'])

            if disc in labeled_discs:
                # imp = injury_data.loc[(injury_data._id == proj) & (injury_data.location == disc) ,'imp']
                
                # if imp.iloc[0] == 1: 
                cases.append('/home/aptus/Clients/Multus/ID_detector/data/staging/' + f)

    train_cases, validation_cases = train_test_split(cases, train_size=0.85, 
                                        random_state=1) 

"""
Get y_true
"""
y_true = []
y_pred = []
for project in validation_cases:
    try:

        # Get input
        inp = input(project)

        # Preprocess
        x1,x2 = preprocess(inp, s_shape, ax_shape)
        inp = [np.array([x1]), np.array([x2])]

        # Save preds
        pred = model.predict(inp)
        pred = np.where(pred>=0.5, 1, 0)

        # Save true value
        true_val = int(output(project, injury_data))

        y_pred.append(int(pred))
        y_true.append(true_val)

    except:
        continue

f1 = f1_score(y_true, y_pred)
acc = accuracy_score(y_true, y_pred)

tn, fp, fn, tp = confusion_matrix(y_true, y_pred).ravel()

name = model_file

print(f'\nEvaluating model: {name}')
print('==============================================')
print(f'TP: {tp}; FP: {fp}; TN: {tn}; FN: {fn}')
print(f'Accuracy score: {acc:.4f}')
print(f'F1 score: {f1:.4f}')
print(f'Positive predictive value (Precision): {(tp/(tp+fp)):.4f}')
print(f'Sensitivity (True positive rate): {(tp/(tp+fn)):.4f}')
print(f'Specificity (True negative rate): {(tn/(tn+fp)):.4f}')

    