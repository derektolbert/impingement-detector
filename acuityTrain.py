import os
import numpy as np
import pandas as pd
from time import gmtime, strftime
import tensorflow as tf
import keras 
from keras import backend as K
from keras.callbacks import ModelCheckpoint
from keras.models import Model, load_model
from keras.layers.advanced_activations import LeakyReLU
from keras.optimizers import Adam
from keras.layers import (
    Dense, Activation, Conv2D, Conv3D, MaxPool2D, MaxPool3D, concatenate, Input, GlobalAveragePooling2D, 
    GlobalAveragePooling3D, Flatten, BatchNormalization, Dropout, GaussianNoise
)
from keras.callbacks import Callback
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score

from acuityGenerator import acuityGenerator

# CONSTANTS
batch_size = 8
s_shape = (batch_size, 15,100,150,1)
ax_shape = (batch_size, 200,200,3)

injury_data = pd.read_csv('impingement_labels.csv')

labeled_projs = list(injury_data._id)

# Get list of all positive cases and split 70%,15%,15% for train, validation, test   
cases = []
for f in os.listdir('/home/aptus/Clients/Multus/ID_detector/data/live_lumbar/'):
    proj = f.split('_')[0]
    disc = f.split('/')[-1].split('_')[1][:-4]
    
    # Only train on labeled projects with labeled discs
    if proj in labeled_projs:
        labeled_discs = list(injury_data.loc[injury_data._id == proj, 'location'])

        if disc in labeled_discs:
            cases.append('/home/aptus/Clients/Multus/ID_detector/data/live_lumbar/' + f)


train_cases, validation_cases = train_test_split(cases, train_size=0.85, 
                                      random_state=1) 

train_batches = acuityGenerator(train_cases, 
    batch_size=batch_size, 
    injury_data=injury_data, 
    sag_input_shape=s_shape, 
    ax_input_shape=ax_shape,
    augment=False,
    resample=False)
validation_batches = acuityGenerator(validation_cases, 
    batch_size=batch_size, 
    injury_data=injury_data, 
    sag_input_shape=s_shape, 
    ax_input_shape=ax_shape)

train_size = 1802
val_size = 218

# Define convolutional blocks 

def sagittal_convolutions(input_tensor):

    # First convolution
    model = Conv3D(filters=16,kernel_size=(3,3,3))(input_tensor)
    model = Conv3D(filters=16,kernel_size=(3,3,3))(model)
    model = LeakyReLU(0.1)(model)
    model = BatchNormalization(axis=-1)(model)
    model = GaussianNoise(stddev=0.01)(model)
    model = MaxPool3D(pool_size=(1,3,3))(model)

    # Second convolution
    model = Conv3D(filters=32,kernel_size=(3,3,3))(model)
    model = Conv3D(filters=32,kernel_size=(3,3,3))(model)
    model = LeakyReLU(0.1)(model)
    model = BatchNormalization(axis=-1)(model)
    model = MaxPool3D(pool_size=(1,3,3))(model)

    # Third convolution
    model = Conv3D(filters=64,kernel_size=(3,3,3))(model)
    model = Conv3D(filters=64,kernel_size=(3,3,3))(model)
    model = LeakyReLU(0.1)(model)
    model = BatchNormalization(axis=-1)(model)
    model = MaxPool3D(pool_size=(1,3,3))(model)

    model = GlobalAveragePooling3D()(model)

    return model

def ax_convolutions(input_tensor):

    # First convolution
    model = Conv2D(filters=32,kernel_size=(3,3))(input_tensor)
    model = Conv2D(filters=32,kernel_size=(3,3))(model)
    model = LeakyReLU(0.1)(model)
    model = BatchNormalization(axis=-1)(model)
    model = GaussianNoise(stddev=0.01)(model)
    model = MaxPool2D(pool_size=(3,3))(model)

    # Second convolution
    model = Conv2D(filters=64,kernel_size=(3,3))(model)
    model = Conv2D(filters=64,kernel_size=(3,3))(model)
    model = LeakyReLU(0.1)(model)
    model = BatchNormalization(axis=-1)(model)
    model = MaxPool2D(pool_size=(3,3))(model)

    # Third convolution
    model = Conv2D(filters=128,kernel_size=(3,3))(model)
    model = Conv2D(filters=128,kernel_size=(3,3))(model)
    model = LeakyReLU(0.1)(model)
    model = BatchNormalization(axis=-1)(model)
    model = MaxPool2D(pool_size=(3,3))(model)

    model = GlobalAveragePooling2D()(model)

    return model

# Create complete model
s_input = Input(batch_shape=s_shape)
s_model = sagittal_convolutions(s_input)

ax_input = Input(batch_shape=ax_shape)
ax_model = ax_convolutions(ax_input)

model = concatenate([s_model, ax_model])
model = Dense(192)(model)
model = Dropout(0.1)(model)
model = LeakyReLU(0.1)(model)

model = Dense(192)(model)
model = LeakyReLU(0.1)(model)
model = Dropout(0.1)(model)

output = Dense(1, activation='sigmoid')(model)
model = Model(inputs=[s_input, ax_input], outputs=[output])

"""
Define F1 score function
"""
def f1(y_true, y_pred):
    def recall(y_true, y_pred):
        """Recall metric.

        Only computes a batch-wise average of recall.

        Computes the recall, a metric for multi-label classification of
        how many relevant items are selected.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    def precision(y_true, y_pred):
        """Precision metric.

        Only computes a batch-wise average of precision.

        Computes the precision, a metric for multi-label classification of
        how many selected items are relevant.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision
    precision = precision(y_true, y_pred)
    recall = recall(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))

"""
Load existing models here if desired
"""
model.load_weights('acuityModels/acuity-F1-0.8287%-0409.hdf5', by_name=True, skip_mismatch=True)

model.summary()
model.compile(loss='binary_crossentropy', optimizer=Adam(lr=0.001), metrics=[f1, 'accuracy'])

timestamp = strftime("%m%d", gmtime())
NAME = './acuityModels/acuity-F1-{val_accuracy:.4f}%-'+timestamp+'.hdf5'

checkpoint = ModelCheckpoint(NAME, monitor='val_f1', save_best_only=True, mode='max')

callbacks = [checkpoint]

class_weights = {0:1, 1:1}

model.fit(train_batches,
                    steps_per_epoch=int(train_size/batch_size),
                    validation_data=validation_batches,
                    validation_steps=int(val_size/batch_size),
                    epochs=10,
                    verbose=1,
                    class_weight=class_weights,
                    callbacks=callbacks)